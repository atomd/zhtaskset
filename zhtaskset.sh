#!/bin/bash

WEIGHT=2
CPU_COUNT=$(cat /proc/cpuinfo | grep processor | wc -l)

n=$#
[ $n -le 0 ] && echo "[ERROR] There are no parameters." && exit 1 

i=0
for cmd in $*
do
    c=0
    res=$(ps ax | grep $cmd | grep -v grep | grep -v $0 | awk {'print $1'})
    for pid in $res
    do
        pids[$i]=$pid
        i=$((i+1))
        c=$((c+1))
    done
    
    echo "request is < $cmd > and related commands is in the following:"
    ps ax | grep $cmd | grep -v grep | grep -v taskset \
          | awk {'sub(/[^\ ]+\ +[^\ ]+\ +[^\ ]+\ +[^\ ]+\ /,"*  "); print'} \
          | sort | uniq
    echo "Are you sure you want to continue(y/n)?"
    read confirm
    if [ $confirm == "n" ]; then
        exit 0
    elif [ $confirm != "y" ]; then
        echo "Please type 'yes' or 'no':"
        read confirm
    fi
    echo ''
done

for((w=$WEIGHT,i=0; w>0; w=w-1))
do
    for((c=CPU_COUNT-1; c>0; c=c-1,i=i+1))
    do
        masks[$i]=$c
    done
done
masks[$i]=0
#echo ${masks[@]}

for((i=0; i<CPU_COUNT; i=i+1))
do
    counts[$i]=0
done

i=0
masks_len=${#masks[@]}
for pid in ${pids[@]}
do
    [ $i -eq $masks_len ] && i=0 
    cid=$(taskset -pc ${masks[$i]} $pid | grep new | awk {'print $6'})
    #taskset -pc ${masks[$i]} $pid 
    if [ "$cid" != ${masks[$i]} ]; then
        echo "[Warn]Something of the process $pid may be wrong, please check!"
        echo ""
    else
        counts[$cid]=$((counts[$cid]+1))
    fi
    i=$((i+1))
done

echo "taskset result:"
for((i=0; i<CPU_COUNT; i=i+1))
do
    echo -e "*  CPU $i:\t${counts[$i]} tasks."
done
